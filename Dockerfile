FROM python:3.8

WORKDIR /holistics-api-poc

COPY requirements.txt .

RUN pip3 install -r requirements.txt

COPY ./main ./main

CMD ["python3", "./main/source.py"]