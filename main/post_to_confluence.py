import requests
import json
import os
import time
import threading
from progressbar import ProgressBar
from datetime import datetime
"""
POST to Confluence REST API.
"""
def post_to_confluence(title, spaceid, pageid, query, msg, diff):
    atlassian_headers = {
        #NVTxui46s5LtJS5NGxy3CB1B
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Basic ZGl4b24uaGFybGV5QHNldGVsLmNvbTpOVlR4dWk0NnM1THRKUzVOR3h5M0NCMUI=",
    }
    atlassian_url = "https://setelnow.atlassian.net/wiki/rest/api/content/"
    payload = json.dumps( {
        "id": "1",
        "title": title,
        "type": "page",
        "space": {
            "key": spaceid
        },
        "status": "current",
        
        "ancestors": [
            {
            "id": pageid
            }
        ],
        
        "body": {
            "storage": {
            "value": "<div class=\"code panel pdl conf-macro output-block\" data-hasbody=\"true\" data-macro-id=\"8373d854-ed21-45af-bcbc-79dd1556df53\" data-macro-name=\"code\" style=\"border-width: 1px;\"><div class=\"codeContent panelContent pdl\">\n<pre class=\"syntaxhighlighter-pre\" data-syntaxhighlighter-params=\"brush: sql; gutter: false; theme: Confluence\" data-theme=\"Confluence\">"+ msg + "</pre>\n</div></div><div class=\"code panel pdl conf-macro output-block\" data-hasbody=\"true\" data-macro-id=\"8373d854-ed21-45af-bcbc-79dd1556df53\" data-macro-name=\"code\" style=\"border-width: 1px;\"><div class=\"codeContent panelContent pdl\">\n<pre class=\"syntaxhighlighter-pre\" data-syntaxhighlighter-params=\"brush: sql; gutter: false; theme: Confluence\" data-theme=\"Confluence\">"+ query + "</pre>\n</div></div><div class=\"code panel pdl conf-macro output-block\" data-hasbody=\"true\" data-macro-id=\"8373d854-ed21-45af-bcbc-79dd1556df53\" data-macro-name=\"code\" style=\"border-width: 1px;\"><div class=\"codeContent panelContent pdl\">\n<pre class=\"syntaxhighlighter-pre\" data-syntaxhighlighter-params=\"brush: sql; gutter: false; theme: Confluence\" data-theme=\"Confluence\">"+ diff + "</pre>\n</div></div>",
            "representation": "storage"
            }
        }
        })
    timeout=(5,25)
    create = requests.request(
        "POST",
        atlassian_url,
        data=payload,
        headers=atlassian_headers,
        timeout=timeout
    )
    return create