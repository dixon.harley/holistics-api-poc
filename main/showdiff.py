import difflib
def _unidiff_output(expected, actual):
    expected = [line for line in expected.split('\n')]
    actual=actual.splitlines(1)
    diff=difflib.unified_diff(expected, actual)
    return ''.join(diff)
