import pytest
from getmodel import getmodel
"""
Test whether the Transform ID inputted by the user matches the GET results
p.s No need mocking here, just GET the results from Holistics API and compare
    Monkeypatching built in python input to "5444". Simulates user input
"""
def test_returnedvalue(monkeypatch):
    monkeypatch.setattr('builtins.input', lambda _: "5444")
    transform = getmodel()
    assert transform['id'] == 5444