import pytest
from fixquery import fixquery
"""
Sample unfixed query
"""
@pytest.fixture
def samplequery():
    return "SELECT *\nFROM\n\tdb1.tb\nWHERE\n\tmetric < 123 && metric > 12"
"""
Sample Fixed query
"""
@pytest.fixture
def fixedquery():
    return "SELECT *<br></br>FROM<br></br>    db1.tb<br></br>WHERE<br></br>    metric &lt; 123 && metric &gt; 12"

def test_query_fixed(samplequery, fixedquery):
    #Test if the function correctly fixes the query
    assert fixquery(samplequery) == fixedquery
    
    

    
