"""
Fix queries retrieved from Holistics, to be HTML friendly for Confluence.
"""
def fixquery(query):
    #Replace <> with HTML friendly entities
    query = query.replace('<', '&lt;')
    query = query.replace('>', '&gt;')
    #Convert Python line breaks to HTML line breaks
    query = query.replace('\n', '<br></br>')
    #Replace Python tab with 4 HTML spaces
    query = query.replace('\t', '    ')
    return query

def undofixquery(query):
    query = query.replace('    ', '\t')
    query = query.replace('<br></br>', '\n')
    query = query.replace('&lt;', '<')
    query = query.replace('&gt;', '>')
    return query