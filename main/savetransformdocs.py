import requests
import json
from fixquery import fixquery
from datetime import datetime
from get_from_holistics import get_from_holistics
from getmodel import getmodel
from post_to_confluence import post_to_confluence

def save_transform():
    atlassian_headers = {
    #NVTxui46s5LtJS5NGxy3CB1B
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Basic ZGl4b24uaGFybGV5QHNldGVsLmNvbTpOVlR4dWk0NnM1THRKUzVOR3h5M0NCMUI=",
    }
    while(True):
        pageid = input("Enter Transform Analysis Page ID: ")
        url = 'https://setelnow.atlassian.net/wiki/rest/api/content/' + pageid + '/child/page?limit=999&start=0&expand=body.view'
        response = requests.get(url, headers=atlassian_headers)
        if response.status_code == 200:
            break
        print("Page not found.")
    response_json = response.json()
    childrens = response_json["results"]
    v0_detected = False
    v0body = ""
    if response_json["size"] == 0:
        v0_detected = False
    else:
        print("1")
        for c in childrens:
            c_split = c['title'].split(" ")
            if c_split[0] == "[v0]":
                v0_detected = True

                
    
    if v0_detected == True:
        while(True):
            ch = input("Version 0 is detected! Commit a Transform? (Y/N)")
            if ch == "Y" or ch == "y":
                model = getmodel()
                msg = input("Input commit message")
                title = str(model['id']) + " " + datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
                query = fixquery(model['query'])
                create = post_to_confluence(title, "~275663773", pageid, query, msg, "diff")
                if create.status_code == 200:
                    print('Success! Title = ' + title)
                    return
                else:
                    print('Error! Status code = ' + create.status_code)
            elif ch == "N" or ch == "n":
                return
    else:
        while(True):
            ch = input("Version 0 is not detected! Commit a Transform as Version 0? (Y/N)")
            if ch == "Y" or ch == "y":
                model = getmodel()
                msg = input("Input commit message")
                title = "[v0] " + str(model['id']) + " " + datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
                query = fixquery(model['query'])
                create = post_to_confluence(title, "~275663773", pageid, query, msg, "diff")
                print('Success! Title = ' + title)
                if create.status_code == 200:
                    print('Success! Title = ' + title)
                    return
                else:
                    print('Error! Status code = ' + create.status_code)
            elif ch == "N" or ch == "n":
                return


