from get_from_holistics import get_from_holistics
import requests
import json
import pandas as pd
import os
import time
import threading
from progressbar import ProgressBar
from sqllineage.runner import LineageRunner
from datetime import datetime

def getmodel():
    response = get_from_holistics()
    holistics_response = response.json()
    while(True):
        modelid = input("Input Transform ID asscociated with the document = ")
        for transform in holistics_response:
            if str(transform['id']) == str(modelid):
                
                return transform
        print("Transform not found")


