import pytest
import requests
from get_from_holistics import get_from_holistics
"""
Check if GET request is successful/not
p.s Again, no need mocks for this test
"""
def test_check_params():
    assert get_from_holistics().status_code == 200