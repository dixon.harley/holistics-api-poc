import pytest
import requests
import requests_mock
import json
import sys
from post_to_confluence import post_to_confluence
"""
Tests whether the POST request is submitted with the right params (payload structure, and headers)
"""
@pytest.mark.parametrize("title, spaceid, pageid, query, msg, diff", [("title", "data", "123", "query", "msg", "diff")])
def test_check_params(title, spaceid, pageid, query, msg, diff):
    """
    Mocks the requests library, any calls to requests will be intercepted and replaced with a mock response
    """
    with requests_mock.mock() as rq:        
        def match_request_text(request):    #Structure to match
            return json.dumps({
                    "id": "1",
                    "title": title,
                    "type": "page",
                    "space": {
                        "key": spaceid
                    },
                    "status": "current",
                    
                    "ancestors": [
                        {
                        "id": pageid
                        }
                    ],
                    
                    "body": {
                        "storage": {
                        "value": "<div class=\"code panel pdl conf-macro output-block\" data-hasbody=\"true\" data-macro-id=\"8373d854-ed21-45af-bcbc-79dd1556df53\" data-macro-name=\"code\" style=\"border-width: 1px;\"><div class=\"codeContent panelContent pdl\">\n<pre class=\"syntaxhighlighter-pre\" data-syntaxhighlighter-params=\"brush: sql; gutter: false; theme: Confluence\" data-theme=\"Confluence\">"+ msg + "</pre>\n</div></div><div class=\"code panel pdl conf-macro output-block\" data-hasbody=\"true\" data-macro-id=\"8373d854-ed21-45af-bcbc-79dd1556df53\" data-macro-name=\"code\" style=\"border-width: 1px;\"><div class=\"codeContent panelContent pdl\">\n<pre class=\"syntaxhighlighter-pre\" data-syntaxhighlighter-params=\"brush: sql; gutter: false; theme: Confluence\" data-theme=\"Confluence\">"+ query + "</pre>\n</div></div><div class=\"code panel pdl conf-macro output-block\" data-hasbody=\"true\" data-macro-id=\"8373d854-ed21-45af-bcbc-79dd1556df53\" data-macro-name=\"code\" style=\"border-width: 1px;\"><div class=\"codeContent panelContent pdl\">\n<pre class=\"syntaxhighlighter-pre\" data-syntaxhighlighter-params=\"brush: sql; gutter: false; theme: Confluence\" data-theme=\"Confluence\">"+ diff + "</pre>\n</div></div>",
                        "representation": "storage"
                        }
                    }
                    }) in (request.text or '')
                
        #Replaces POST method with mocked one
        rq.post('https://setelnow.atlassian.net/wiki/rest/api/content/', 
            request_headers={"Accept": "application/json", 
                            "Content-Type": "application/json", 
                            "Authorization": "Basic ZGl4b24uaGFybGV5QHNldGVsLmNvbTpOVlR4dWk0NnM1THRKUzVOR3h5M0NCMUI="}, 
            text='resp',
            additional_matcher=match_request_text,
            status_code=200)
        
        #rq.post('https://setelnow.atlassian.net/wiki/rest/api/content/', status_code=404) -- 404 with invalid parms (WIP)
        response = post_to_confluence(title, spaceid, pageid, query, msg, diff)
    print(response.status_code)
    assert response.status_code == 200
"""
Test load retrieved from the API == load returned by the function
"""
@pytest.mark.parametrize("title, spaceid, pageid, query, msg, diff", [("title", "data", "123", "query", "msg", "diff")])
def test_return_correct_payload(title, spaceid, pageid, query, msg, diff):
    with requests_mock.mock() as rq:
        rq.post('https://setelnow.atlassian.net/wiki/rest/api/content/',  
            text='resp')
        response = post_to_confluence(title, spaceid, pageid, query, msg, diff)
    assert response.text == 'resp'


