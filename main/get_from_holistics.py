import requests
import json
import os
from progressbar import ProgressBar
from sqllineage.runner import LineageRunner
from datetime import datetime
"""
GET Data Transform list from Holistics API
"""
def get_from_holistics():
    hol_headers = {
        'Content-type': 'application/json',
        'X-Holistics-Key' :'0YVdULBAtMZBxwP0i8ozKdlByZE8ad+JmGjlyQD56u4=' }
    hol_url = 'https://secure.holistics.io/data_transforms.json'
    response = requests.get(hol_url, headers = hol_headers)
    return response